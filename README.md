# GetAGriffin Product Suite

This repo is the main (product level) repo for the examples used in the O'Reilly/Pearson online course, *More Effective DevOps Testing*. The course's home page is [here](https://agilegriffin.com/oreilly), which contains a link to the [setup instructions](https://agilegriffin.com/training-more-effective-devops-testing). As explained in the setup instructions, the scrips use Java, Maven, Docker, and Docker Compose. All tools are installed in a local virtual machine via Vagrant, using the Vagrantfile in this directory, which you should copy one level up in order to ensure that Vagrant maps the enclosing directory (rather than this directory) to the VM. Thus, to install this course's files, the procedure is:
1. Perform the [setup instructions](https://agilegriffin.com/training-more-effective-devops-testing).
2. Create a directory for this course. Say it is called "DevOps".
3. Go into that directory with a Windows or Mac terminal.
4. Clone each of the five repos, using the git clone command.
5. Copy this repo's Vagrantfile up one level, so that it is in the DevOps directory.
6. From the DevOps directory, run "vagrant up" to launch a VM, which will map your current directory (DevOps) to the VM's /vagrant directory.

There are five repos in total: this one, and one for each of the four components (see below).

The product uses a makefile. This is so that various steps can be performed that are not directly
supported by Maven.

The makefile assumes that all five repos are cloned into the same master directory, so that it contains
a folder for the product and a folder for each component.

This repo (the product level repo) integrates various components into a
user-facing, value-producing product. It contains:

* Vagrantfile - for standup up a local VM that can be used for local functional
integration testing.
* toolchain-vm.xml - A toolchain configuration for the above VM. The Vagrantfile
automatically install this file in the VM.
* Product level test suites (implemented as Maven modules and run by Cucumber),
per the test strategy (see below).

## Component Repos

This product is composed of component, contained in the following component repos:

* [UI](https://gitlab.com/cliffbdf/getagriffin-ui)
* [Order](https://gitlab.com/cliffbdf/getagriffin-order)
* [FulfillGriffin](https://gitlab.com/cliffbdf/getagriffin-fulfillgriffin)
* [FulfillSaddle](https://gitlab.com/cliffbdf/getagriffin-fulfillsaddle)

Each component repo has a component level test strategy, for that component. However,
a component should not be considered to be tested until its component level tests pass,
and the product level tests also pass for any products in which the component is used.

## Other Repos Needed

The following repo is needed for this product. It is not in Maven Central, so you will need
to clone it and build it, by running `make install`, which will build it and install it
in your local maven repository:

https://gitlab.com/cliffbdf/utilities-java

## To Deploy All components

### Locally

```
cd /vagrant/getagriffin
make deploy
```

The makefile `deploy` task uses the deployment scripts for each respective component, and adds those components to a single "back end" network called "getagriffin" so that they can call each other. The figure below shows the resulting networks and containers running within your computer's virtual environment.

![Deployment Configuration Using Docker Compose](ProductLevelComposeTestConfig.png)

### In AWS

TBD

## To Run Test Suites

After deploying for test, one may run the test suites:

### Locally

```
# Run behavioral integration tests
make test211

# Run saga level tests
mvn --projects sagas compiler:testCompile failsafe:integration-test

# Run failure mode tests
make failtests

# Clean up
make destroy
```

### Under Jenkins

TBD

## Product Level Test Strategy

1. Integration test (thorough coverage):
	1. Order -> ((FulfillGriffin -> DB), (FulfillSaddle -> DB))
2. Integration test (light coverage):
	1. UI -> Order -> ((FulfillGriffin -> DB), (FulfillSaddle -> DB))
3. Failure mode test (local):
	1. Component failure: Order’s DB becomes unreachable
	2. Component failure: FulfillGriffin hangs
	3. Saga failure: Order update succeeds but FulfillGriffin update fails
4. Dynamic security scan (local)
5. Stress test (Jenkins)

### Component Level Tests

Each component repo contains test suites for that component. These include,

1. API test of microservice Order (with FulfillGriffin and FulfillSaddle mocked)
2. API test of microservice FulfillGriffin
3. API test of microservice FulfillSaddle

## External Packages used

The microservices in this product use the SparkJava framework, which is a much
lighterweight, less opinionated alternative to Spring. It is also easier for those
new to the framework to understand the code. The documentation for SparkJava
can be found here: http://sparkjava.com/documentation
