@Failure
@Component
Feature: FailureModes

	Scenario: FulfillSaddle hangs

		Given that FulfillSaddle is scaled to 1 container
		And FulfillSaddle has hung
		And at least 1000 milliseconds have passed since it hung

		When I access FulfillSaddle

		Then it has recovered and I receive a non-error result
