package getagriffin.container;

public interface ContainerProvider {
	/** Obtain an array of the container Ids of the running instances of the specified image. */
	String[] getContainerIds(String imageName) throws Exception;

	/** Cause the container with the specified ID to stop running. */
	void stopContainer(String containerId) throws Exception;
}
