package getagriffin.container;

import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.List;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

public class ComposeContainerProvider implements ContainerProvider {

	public ComposeContainerProvider() {
	}

	/** Obtain an array of the container Ids of the running instances of FulfillSaddle.
	If none are found, return an empty array.
	Ref: https://docs.docker.com/engine/reference/commandline/ps/#filtering
	*/
	public String[] getContainerIds(String imageName) throws Exception {
		//String cmd = "/usr/bin/docker container ls --filter ancestor=" + imageName;
		BufferedReader br = perfProcessSync(
			"/usr/bin/docker", "container", "ls", "--filter", "name=" + imageName);

		/* Read and parse the command's output: */
		List<String> ids = new LinkedList<String>();
		for (;;) {
			String line = br.readLine();
			if (line == null) break; // no more lines
			if (line.contains("CONTAINER ID")) continue;
			String regex = "^[^ \t]+";
			String[] parts = line.split(regex);
			if (parts.length == 0) continue;
			String id = parts[0];
			ids.add(id);
		}

		return ids.toArray(new String[ids.size()]);
	}

	/** Cause the container with the specified ID to stop running. */
	public void stopContainer(String containerId) throws Exception {
		String cmd = "/usr/bin/docker stop " + containerId;
		perfProcessSync("/usr/bin/docker", "stop", containerId);
	}

	/**
	Execute an operating system shell command and wait for it to complete.
	*/
	private BufferedReader perfProcessSync(String... cmd)
		throws TimeoutException, ExitValueException, InterruptedException, IOException {

		ProcessBuilder pb = new ProcessBuilder(cmd);
		Process p = pb.start();
		InputStream is = p.getInputStream();  // the process output
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		boolean completed = p.waitFor(1000, TimeUnit.MILLISECONDS);
		if (! completed) {
			throw new TimeoutException();
		}
		if (p.exitValue() != 0) {
			System.err.println("Process exited with non-zero status. Here is the process stdout:\n");
			for (;;) {
				String line = br.readLine();
				if (line == null) break;
				System.err.println(line);
			}
			System.err.println("--End of process stdout--");
			throw new ExitValueException();
		}
		return br;
	}

	public class TimeoutException extends Exception {}

	public class ExitValueException extends Exception {}
}
